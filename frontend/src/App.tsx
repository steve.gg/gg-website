import './App.css';
import { HomePageComponent } from './components/home/home';

function App() {
  return (
    <div className="App">
      <HomePageComponent />
    </div>
  );
}

export default App;
