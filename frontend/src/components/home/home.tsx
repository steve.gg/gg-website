import * as React from "react";
import './home.css';
import { NavigationComponent } from "../navigation/navigation";

interface HomePageProps {

}

const HomePageComponent: React.FC<HomePageProps> = (props: HomePageProps) => {

    return (
        <>
            <NavigationComponent links={{About: '#', Skills: '#', Projects: '#', Volunteer: '#',    Contact: '#'}}/>
        </>
    );
}

export {
    HomePageComponent
};
