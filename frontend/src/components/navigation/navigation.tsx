import * as React from 'react';
import './navigation.css';

interface NavigationProps {
    links: { [key: string]: string }
}

const NavigationComponent: React.FC<NavigationProps> = (props: NavigationProps) => {
    const components: React.ReactElement[] = [];

    for (const key in props.links) {
        const url = props.links[key];
        const component = getNavComponent(key, url);
        components.push(component);
    }

    return (
        <>
            <div className='navigation'>
                <div className='navigation-elements-container'>
                    {components.map(component => component)}
                </div>
            </div>
        </>
    );
}

const getNavComponent = (key: string, url: string): React.ReactElement => (
    <div className={`navigation-element`} key={`navigation-element-${key}`}>
            <a href={url}>{key}</a>
    </div>
);

export {
    NavigationComponent
};
